package com.example.userapp.test;



import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@SpringBootTest
public class UserAppTests {

		TestRestTemplate restTemplate = new TestRestTemplate();
		HttpHeaders headers = new HttpHeaders();

		@Test
		public void testGetUserById() throws JSONException {

			HttpEntity<String> entity = new HttpEntity<String>(null, headers);

			ResponseEntity<String> response = restTemplate.exchange(
					createURLWithPort("/api/users/8"),
					HttpMethod.GET, entity, String.class);

			String expected = "{id:8,name:rrrr,lastname:rrrrr,createdAt:1518679999000,updatedAt:1518680060000}";

			JSONAssert.assertEquals(expected, response.getBody(), false);
		}

		private String createURLWithPort(String uri) {
			return "http://localhost:8080" + uri;
		}

		
	}


